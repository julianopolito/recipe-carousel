import { RecommendationsConfig, recommend } from "recs";

export class BannerWithCTATemplate implements CampaignTemplateComponent {
    @title("CTA Text")
    ctaText:String = "Buy Now"; 

    /**
     * Developer Controls
     */

    @hidden(true)
    maximumNumberOfProducts = 5;

    /**
     * Business-User Controls
     */

    @title(" ")
    recsConfig: RecommendationsConfig = new RecommendationsConfig()
        .restrictItemType("Product")
        .restrictMaxResults(this.maximumNumberOfProducts);

    @header("Customization Options")
    
    brandName:String = "Salesforce";

    prodBgColor:Color;
    imgBgColor:Color;

    run(context: CampaignComponentContext) {
        this.recsConfig.maxResults = this.maximumNumberOfProducts;
        return { 
            rec: recommend(context, this.recsConfig),
            itemType: this.recsConfig.itemType
        };
    }

}


