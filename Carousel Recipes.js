(function() {

    /**
     * @function buildBindId
     * @param {Object} context
     * @description Create unique bind ID based on the campaign and experience IDs.
     */
    function buildBindId(context) {
        return `${context.campaign}:${context.experience}`;
    }

    function apply(context, template) {
        const contentZoneSelector = Evergage.getContentZoneSelector(context.contentZone);
        console.log("apply running", template(context));
        console.log("carousel prods: ", context.products);
        /**
         * The pageElementLoaded method waits for the content zone to load into the DOM
         * before rendering the template. The observer element that monitors for the content
         * zone element to get inserted into its DOM node is set to "body" by default.
         * For performance optimization, this default can be overridden by adding
         * a second selector argument, which will be used as the observer element instead.
         *
         * Visit the Template Display Utilities documentation to learn more:
         * https://developer.evergage.com/campaign-development/web-templates/web-display-utilities
         */
        return Evergage.DisplayUtils
            .bind(buildBindId(context))
            .pageElementLoaded(contentZoneSelector)
            .then((element) => {
                const html = template(context);
                Evergage.cashDom(element).html(html);
                Evergage.cashDom(".carControl").first().trigger("click");
                document.documentElement.style.setProperty('--prod-bg-color',context.prodBgColor.hex);
                var pc = context.prodBgColor;
                var luminance = 0.2126*pc.r + 0.7152*pc.g + 0.0722*pc.b;
                if(luminance > 150){
                    document.documentElement.style.setProperty('--button-text-color',"#000");
                }else{
                    document.documentElement.style.setProperty('--button-text-color',"#fff");
                }
                console.log("luminance ", luminance);
                document.documentElement.style.setProperty('--img-bg-color',context.imgBgColor.hex);
                document.documentElement.style.setProperty('--brand-text',context.brandName);

                console.log("returning template ", element, html);
            }); 
    }

    function reset(context, template) {
        Evergage.DisplayUtils.unbind(buildBindId(context));
        Evergage.cashDom("#carContainer").remove();
    }

    function control(context) {
        
    }

    registerTemplate({
        apply: apply,
        reset: reset,
        control: control
    });

})();
